class ProjectsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_if_professor, only: [:new, :create]
  before_action :check_if_is_manager, only: [:edit, :update, :destroy]
  before_action :set_project, only: [:show, :edit, :update, :destroy]

  # GET /projects
  # GET /projects.json
  def index
    query = params['query']
    @projects = Project.text_search(query)
                        .joins(:members)
                        .where(members: {user_id: current_user.id})
                        .order("last_update desc")
                        .paginate(:page => params[:page])
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @tasks_in_progress = Task.in_progress_list(nil, @project)

    @tasks_done = Task.done_list(nil, @project)

    @tasks_pending = Task.pending_list(nil, @project)

    @tasks_total_count = (@tasks_in_progress.count + @tasks_pending.count + @tasks_done.count)
    if(@tasks_total_count > 0)
      @done_tasks_percentage = @tasks_done.count * 100 / @tasks_total_count
    else
      @done_tasks_percentage = 0
    end
  end

  # GET /projects/new
  def new
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        @project.add_member(current_user, is_manager: true, is_scholar: false)
        format.html { redirect_to @project, notice: 'Projeto foi criado com sucesso.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Projeto foi atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Projeto foi apagado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, :description, :last_update)
    end

    def check_if_is_manager
      @project = Project.find(params[:id])
      unless current_user.is_manager?(@project)
        flash[:alert] = 'Você não tem permissão para isso'
        redirect_to projects_path
      end
    end

    def check_if_professor
      unless current_user.is_professor
        flash[:alert] = 'Você não tem permissão para isso'
        redirect_to projects_path
      end
    end
end
