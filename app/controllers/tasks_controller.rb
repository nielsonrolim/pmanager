class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy, :new_member, :add_member, :remove_member]

  # GET /tasks
  # GET /tasks.json
  def index
     @tasks_done = Task.done_list(current_user, nil)
     @tasks_in_progress = Task.in_progress_list(current_user, nil)
     @tasks_pending = Task.pending_list(current_user, nil)
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
    @project = Project.find params[:project_id]
  end

  # GET /tasks/1/edit
  def edit
    @project = Project.find params[:project_id]
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(task_params)

    respond_to do |format|
      if @task.save
        @task.add_member(current_user)
        format.html { redirect_to @task, notice: 'Tarefa criada com sucesso.' }
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to @task, notice: 'Task was successfully updated.' }
        format.json { render :show, status: :ok, location: @task }
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to tasks_url, notice: 'Task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  # POST /task/1/add_comment
  def add_comment
       @task = Task.find params[:id]
       member = Member.where(project_id: @task.project_id, user_id: current_user.id).first
       @task.add_comment(title: params[:title], content: params[:content], member_id: member.id)
       redirect_to task_path(@task), notice: "Comentário adicionado"
  end

  # GET /tasks/1/member/new
  def new_member
    @query = params['query']
    @users = User.select("users.id, users.name, users.email, users.university")
                    .joins(:members)
                    .where('members.project_id': @task.project_id)
                    .where.not('members.id' => MemberTask.select('member_id').where(task_id: @task.id))
    if @query.present?
      @users = @users.text_search(@query)
    end
  end

  # POST /tasks/1/member/1/add
  def add_member
    user = User.find params[:user_id]
    @task.add_member(user)
    respond_to do |format|
      format.html { redirect_to task_path(params[:id]), notice: user.name + " adicionado à tarefa " + @task.title + " com sucesso."}
      format.json { head :no_content }
    end
  end

  # DELETE /tasks/1/member/1/remove
  def remove_member
    member = Member.find params[:member_id]
    @task.members.delete(member)

    respond_to do |format|
      format.html { redirect_to task_path(@task), notice: "#{member.user.name} foi removido da tarefa com sucesso." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:title, :description, :due_date, :status, :project_id)
    end

end
