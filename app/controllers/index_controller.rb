require 'date'

class IndexController < ApplicationController
   before_action :authenticate_user!, except: [:test]
   skip_before_action :verify_authenticity_token

  def index
     @tasks_done = Task.done_list(current_user, nil)
     @tasks_pending = Task.pending_list(current_user, nil)
     @tasks_in_progress = Task.in_progress_list(current_user, nil)

     @tasks_total = (@tasks_pending.count + @tasks_in_progress.count + @tasks_done.count);
     if(@tasks_total > 0)
       @done_tasks_percentage = @tasks_done.count * 100 / @tasks_total
     else
       @done_tasks_percentage = 0
     end

     @projects_pending_or_in_progress = Project.joins(:members)
                           .where("members.user_id = #{current_user.id}")
                           .joins(:tasks)
                           .where("tasks.status != 'Concluída'").uniq

     @projects_all = Project.joins(:members)
                           .where("members.user_id = #{current_user.id}")
     @projects_done = @projects_all.count - @projects_pending_or_in_progress.count

     if(@projects_all.count > 0)
       @projects_done_percentage = @projects_done * 100 / @projects_all.count
     else
       @projects_done_percentage = 0
     end

     @events = Event.joins(:events_users)
                    .where("events_users.user_id = #{current_user.id} AND extract(month from date) = #{Date.today.strftime("%m")}")
                    .order("date desc")
  end

  def demo
  end

  def about
  end

  def test
    respond_to do |format|
      format.html
      format.json
    end
  end
end
