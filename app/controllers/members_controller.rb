class MembersController < ApplicationController
  before_action :set_member, only: [:show, :edit, :update, :destroy]

  # GET /members
  # GET /members.json
  def index
    @project = Project.find params[:project_id]
    @members = Member.joins(:user).where(project_id: @project.id).order("users.name asc")
    @managers = Member.all.where("members.project_id = #{@project.id} AND members.is_manager = true")
  end

  # GET /projects/new
  def new
    @project = Project.find params[:project_id]
    @query = params['query']
    if @query.present?
      @users = User.select("id, name, email, university")
                    .where.not(id: Member.select("user_id").where(project_id: @project.id))
                    .text_search(@query)
    else
      @users = []
    end
  end

  # POST /projects
  def add
    @project = Project.find params[:project_id]
    user = User.find params[:user_id]
    @project.add_member(user, is_scholar: params[:is_scholar], is_manager: params[:is_manager])
    respond_to do |format|
      format.html { redirect_to project_members_path(params[:project_id]), notice: user.name + " adicionado ao " + @project.name }
      format.json { head :no_content }
    end
  end

  # DELETE /members/1
  # DELETE /members/1.json
  def destroy
    @managers = Member.all.where("members.project_id = #{params[:project_id]} AND members.is_manager = true")
    if @member.is_manager && @managers.count == 1
        notice = 'O membro não pode ser removido, este é o único gerente do projeto.'
    else
      @member.destroy
      notice = 'Membro removido com sucesso.'
    end
    respond_to do |format|
      format.html { redirect_to project_members_path(params[:project_id]), notice: notice }
      format.json { head :no_content }
    end
  end

  def toggle_is_scholar
    member = Member.find(params[:member_id])
    refresh = member.is_manager
    member.update_attributes(:is_scholar => params[:is_scholar])
    render nothing: true
  end

  def toggle_is_manager
    member = Member.find(params[:member_id])
    number_of_managers = Member.all.where("members.project_id = #{member.project_id} AND members.is_manager = true")
    refreshPage = ((member.is_manager && number_of_managers.count == 2) || number_of_managers.count == 1)

    member.update_attributes(:is_manager => params[:is_manager])

    if refreshPage
      respond_to do |format|
          format.js {render inline: "location.reload();" }
      end
    else
      render nothing: true
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member
      @member = Member.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def member_params
      params.require(:member).permit(:is_scholar, :is_manager)
    end
end
