class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy, :new_user, :add_user, :remove_user]
  before_action :check_if_owner, only: [:edit, :update, :destroy]

  # GET /events
  # GET /events.json
  def index
    @events = Event.joins(:events_users)
                   .where("events_users.user_id = #{current_user.id} OR events.is_private = false")
                   .order("date desc")
                   .paginate(:page => params[:page])
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @users = User.joins(:events).where(events_users: {event_id: @event.id})
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.owner = current_user

    respond_to do |format|
      if @event.save
        @event.add_user(current_user)
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /events/1/user/new
  def new_user
    @query = params['query']
    if @query.present?
      @users = User.select("id, name, email, university")
                    .where.not(id: EventUser.select("user_id").where(event_id: @event.id))
                    .text_search(@query)
    else
      @users = []
    end
  end

  # POST /events/1/user/1/add
  def add_user
    user = User.find params[:user_id]
    @event.add_user(user)
    respond_to do |format|
      format.html { redirect_to event_path(@event), notice: user.name + " adicionado ao evento " + @event.title + " com sucesso." }
      format.json { head :no_content }
    end
  end

  # DELETE /events/1/user/1/remove
  def remove_user
    user = User.find params[:user_id]
    @event.remove_user(user)
    
    respond_to do |format|
      format.html { redirect_to event_path(@event), notice: "#{user.name} foi removido do evento com sucesso." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:title, :description, :date, :location, :location_url, :kind, :is_private, :owner_id)
    end

    def check_if_owner
      unless current_user.owns_event?(@event)
        flash[:alert] = 'Você não tem permissão para isso'
        redirect_to events_path
      end
    end

end
