var members_ready;
members_ready = function() {

	$(".is_scholar-check").bind('change', function(){
	    $.ajax({
	      url: '/members/' + this.value + '/toggle_is_scholar',
	      type: 'POST',
	      data: {"is_scholar": this.checked}
	    });
	});

	$(".is_manager-check").bind('change', function(){
	    $.ajax({
	      url: '/members/' + this.value + '/toggle_is_manager',
	      type: 'POST',
	      data: {"is_manager": this.checked}
	    });
	});

};

$(document).ready(members_ready);
$(document).on('page:load', members_ready);