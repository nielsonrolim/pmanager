class Event < ActiveRecord::Base
  has_and_belongs_to_many :users
  belongs_to :owner, class_name: 'User', foreign_key: :owner_id

  validates :title, :date, presence: true

  def add_user(user, attributes={})
  	if self.users.include?(user)
      false
    else
      self.users << user
    end
  end

  def remove_user(user)
  	self.users.delete(user)
  end
end
