class Member < ActiveRecord::Base
  belongs_to :user
  belongs_to :project
  has_many :comments
  has_and_belongs_to_many :tasks

end
