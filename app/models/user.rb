class User < ActiveRecord::Base
  include PgSearch

  has_many :members
  has_many :projects, through: :members
  has_and_belongs_to_many :events
  has_many :owned_events, class_name: 'Event', foreign_key: :owner_id


  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, :email, presence: true
  validates :email, :format => /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/

  pg_search_scope :search, against: [:name, :email, :university],
                  using: {tsearch: {dictionary: "portuguese"}},
                  ignoring: :accents

  def is_manager?(project)
  	member = Member.where(user: self.id, project: project).first.is_manager
  end

  def self.text_search(query)
    if query.present?
      search(query)
    else
      self.all
    end
  end

  def owns_event?(event)
    self.owned_events.include?(event)
  end

end
