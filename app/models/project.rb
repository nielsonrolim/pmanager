class Project < ActiveRecord::Base
  include PgSearch
  
  after_create :init_last_update

  has_many :tasks
  has_many :members, dependent: :destroy
  has_many :users, through: :members

  self.per_page = 10

  validates :name, presence: true
  validates :name, uniqueness: true

  pg_search_scope :search, against: [:name, :description],
                  using: {tsearch: {dictionary: "portuguese"}},
                  ignoring: :accents


  def add_member(user, attributes={})
    if self.users.include?(user)
      false
    else
      self.members << Member.create(user: user, project: self, is_manager: attributes[:is_manager], is_scholar: attributes[:is_scholar])
    end
  end

  def self.text_search(query)
    if query.present?
      search(query)
    else
      self.all
    end
  end

  private

  def init_last_update
    self.last_update = created_at
    self.save!
  end
end
