class Comment < ActiveRecord::Base
  belongs_to :task
  belongs_to :member

  validates :title, :content, :date, :task_id, :member_id, presence: true
end
