class Task < ActiveRecord::Base
  belongs_to :project
  has_many :comments, dependent: :destroy
  has_and_belongs_to_many :members

  validates :title, :description, presence: true

  self.per_page = 10


  def add_member(user, attributes={})
    if self.members.include?(user) or not self.project.users.include?(user)
      false
    else
      self.members << Member.where(project_id: self.project_id, user_id: user.id)
    end
  end

  def add_comment(attributes={})
      self.comments << Comment.create(member_id: attributes[:member_id], task_id: self.id, title: attributes[:title], content: attributes[:content])
  end

  def set_project(project, attributes={})
    self.project = project
  end

  def self.done_list(user, project)
    if project == nil
      Task.joins(:members)
          .where("members.user_id = #{user.id} AND tasks.status = 'Concluída'")
          .order("due_date asc")
    else
      Task.where("tasks.project_id = #{project.id} AND tasks.status = 'Concluída'")
          .order("due_date asc")

    end
  end

  def self.pending_list(user, project)
    if project == nil
      Task.joins(:members)
          .where("members.user_id = #{user.id} AND tasks.status = 'Pendente'")
          .order("due_date asc")
    else
      Task.where("tasks.project_id = #{project.id} AND tasks.status = 'Pendente'")
          .order("due_date asc")
    end
  end

  def self.in_progress_list(user, project)
    if project == nil
      Task.joins(:members)
          .where("members.user_id = #{user.id} AND tasks.status = 'Em andamento'")
          .order("due_date asc")
    else
      Task.where("tasks.project_id = #{project.id} AND tasks.status = 'Em andamento'")
          .order("due_date asc")
    end
  end

end
