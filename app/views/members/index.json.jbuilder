json.array!(@members) do |member|
  json.extract! member, :id, :is_scholar, :is_manager
  json.url member_url(member, format: :json)
end
