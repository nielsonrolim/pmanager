json.extract! @event, :id, :title, :description, :date, :location, :location_url, :kind, :is_private, :created_at, :updated_at
