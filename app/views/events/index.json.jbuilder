json.array!(@events) do |event|
  json.extract! event, :id, :title, :description, :date, :location, :location_url, :kind, :is_private
  json.url event_url(event, format: :json)
end
