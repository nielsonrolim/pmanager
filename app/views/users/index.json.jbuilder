json.array!(@users) do |user|
  json.extract! user, :id, :email, :name, :university, :phone, :is_professor
  json.url user_url(user, format: :json)
end
