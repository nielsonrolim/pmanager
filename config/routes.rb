Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }
  get 'index/index'
  get 'demo' => 'index#demo'

  get 'index/test'
  post 'index/test'

  get 'about' => 'index#about', as: :about
  get 'dashboard' => 'index#index', as: :dashboard

  get 'project/:project_id/members' => 'members#index', as: :project_members
  get 'project/:project_id/members/new' => 'members#new', as: :new_project_member
  post 'project/:project_id/members/add' => 'members#add', as: :add_project_member
  delete 'project/:project_id/members/:id/remove' => 'members#destroy', as: :project_remove_member

  post 'members/:member_id/toggle_is_scholar' => 'members#toggle_is_scholar', as: :toggle_is_scholar
  post 'members/:member_id/toggle_is_manager' => 'members#toggle_is_manager', as: :toggle_is_manager

  get 'project/:project_id/tasks' => 'tasks#index', as: :project_task
  get 'project/:project_id/tasks/new' => 'tasks#new', as: :new_project_task
  get 'project/:project_id/tasks/:id/edit' => 'tasks#edit', as: :edit_project_task
  post 'project/:project_id/tasks/create' => 'tasks#create', as: :create_project_task
  delete 'project/:project_id/tasks/:id/remove' => 'tasks#destroy', as: :project_remove_task
  get 'tasks/:id/member/new' => 'tasks#new_member', as: :new_task_member
  post 'tasks/:id/member/:user_id/add' => 'tasks#add_member', as: :add_task_member
  delete 'tasks/:id/member/:member_id/remove' => 'tasks#remove_member', as: :task_remove_member

  delete 'events/:id/user/:user_id/remove' => 'events#remove_user', as: :event_remove_user
  get 'events/:id/user/new' => 'events#new_user', as: :new_event_user
  post 'events/:id/user/:user_id/add' => 'events#add_user', as: :add_event_user

  post 'tasks/:id/add_comment' => 'tasks#add_comment', as: :task_add_comment

  resources :projects
  resources :members
  resources :tasks
  resources :events
  resources :users

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'index#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
