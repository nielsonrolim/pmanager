# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

=begin
Rails.application.config.assets.precompile += %w( bootstrap/dist/fonts/glyphicons-halflings-regular.eot )
Rails.application.config.assets.precompile += %w( bootstrap/dist/fonts/glyphicons-halflings-regular.woff2 )
Rails.application.config.assets.precompile += %w( bootstrap/dist/fonts/glyphicons-halflings-regular.woff )
Rails.application.config.assets.precompile += %w( bootstrap/dist/fonts/glyphicons-halflings-regular.ttf )
Rails.application.config.assets.precompile += %w( bootstrap/dist/fonts/glyphicons-halflings-regular.svg )

Rails.application.config.assets.precompile += %w( font-awesome/css/font-awesome/FontAwesome.otf )
Rails.application.config.assets.precompile += %w( font-awesome/css/font-awesome/fontawesome-webfont.eot )
Rails.application.config.assets.precompile += %w( font-awesome/css/font-awesome/fontawesome-webfont.svg )
Rails.application.config.assets.precompile += %w( font-awesome/css/font-awesome/fontawesome-webfont.ttf )
Rails.application.config.assets.precompile += %w( font-awesome/css/font-awesome/fontawesome-webfont.woff )
=end

