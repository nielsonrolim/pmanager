require 'test_helper'

class IndexControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  test "should get index" do
    sign_in users(:maicon)
    get :index
    assert_response :success
  end

end
