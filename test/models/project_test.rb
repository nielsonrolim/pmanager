require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "project attributes must not be empty" do
    project = Project.new
    assert project.invalid?
    assert project.errors[:name].any?
  end

  test "project is not valid without a unique name" do
    project = Project.new(name: projects(:manhattan).name)

    assert project.invalid?, "Project is not invalid"
    assert_equal [I18n.translate('errors.messages.taken')], project.errors[:name], "Project's name is not unique"
  end

end
