require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end


  test "task attributes must not be empty" do
    task = Task.new
    assert task.invalid?
    assert task.errors[:title].any?
    assert task.errors[:description].any?
    assert task.errors[:project_id].any?
  end

end
