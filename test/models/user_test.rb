require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "user attributes must not be empty" do
    user = User.new
    assert user.invalid?
    assert user.errors[:name].any?
    assert user.errors[:email].any?
  end

  test "user is not valid without a unique registration number" do
    user = User.new(registration_number: users(:maicon).registration_number,
                    name: 'Pedro Bó',
                    email: 'pedrobo@chicocity.com.br',
                    password: 'something')

    assert user.invalid?, "User is not invalid"
    assert_equal [I18n.translate('errors.messages.taken')], user.errors[:registration_number], "User's registration_number is not unique"
  end

  test "user is valid without a registration number" do
    user = User.new(name: 'Pedro Bó',
                    email: 'pedrobo@chicocity.com.br',
                    password: 'something')
    assert user.valid?, "User is not valid"
  end
end
