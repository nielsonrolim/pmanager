require 'test_helper'

class EventTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "event attributes must not be empty" do
    event = Event.new
    assert event.invalid?
    assert event.errors[:title].any?
    assert event.errors[:date].any?
  end
end
