require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "comment attributes must not be empty" do
    comment = Comment.new
    assert comment.invalid?
    assert comment.errors[:title].any?
    assert comment.errors[:content].any?
    assert comment.errors[:date].any?
    assert comment.errors[:task_id].any?
    assert comment.errors[:member_id].any?
  end
end
