class CreateJoinTableMemberTask < ActiveRecord::Migration
  def change
    create_join_table :members, :tasks do |t|
      t.index [:member_id, :task_id]
      # t.index [:task_id, :member_id]
    end
  end
end
