class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.boolean :is_scholar, default: false
      t.boolean :is_manager, default: false
      t.belongs_to :project, index: true, foreign_key: true
      t.belongs_to :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
