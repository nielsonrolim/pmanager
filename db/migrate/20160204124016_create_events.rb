class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.datetime :date
      t.string :location
      t.string :location_url
      t.string :type
      t.boolean :is_private, default: false

      t.timestamps null: false
    end
  end
end
