class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :description
      t.datetime :due_date
      t.string :status, default: 'todo'
      t.datetime :finished_date
      t.integer :project_id

      t.timestamps null: false
    end
  end
end
