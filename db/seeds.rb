# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

nielson = User.create!(name: "Nielson Rolim", email: 'nielson@bitmine.com.br', password: '12345678', is_professor: true)
felipe = User.create!(name: "Felipe Porge", email: 'felipe@bitmine.com.br', password: '12345678', is_professor: true)
bilolinha = User.create!(name: "Bilolinha", email: 'bilolinha@bitmine.com.br', password: '12345678')
bolsista = User.create!(name: "Bolsista", email: 'bolsista@bitmine.com.br', password: '12345678')

(1..20).each do |i|
  p = Project.create(name: "Project " + i.to_s, description: "Description of project " + i.to_s )
  
  #Regular Memeber
  p.members << Member.create(user: bilolinha, project: p, is_manager: false, is_scholar: false)
  p.members << Member.create(user: bolsista, project: p, is_manager: false, is_scholar: true)

  #Managers
  p.members << Member.create(user: nielson, project: p, is_manager: true)
  if i % 2 == 0
  	p.members << Member.create(user: felipe, project: p, is_manager: true)
  end

end
